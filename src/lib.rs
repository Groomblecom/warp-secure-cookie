//! warp_secure_cookie
//! ---
//!
//! This crate is a fairly simple extension to the Warp web server to include
//! secure cookies. The cookies provided by this crate are _not_ encrypted,
//! but rather tamper-evident using an HMAC mechanism.
//!
//! It uses OpenSSL to make HMACs; the user is responsible for configuring
//! a suitible Signer, though a convenience method for a sensible default
//! is provided. 
//!
//! OpenSSL was selected over Ring because Linux distro
//! maintainers generally and correctly do not permit statically-linked 
//! non-standard crypto libraries in their packages. While the base Warp 
//! server does use Ring for SSL, you can avoid Warp SSL altogether
//! and tell users to proxy with NGINX or similar.
//!
//! To avoid Nefarious Shenanigans, we mangle the hash of the name onto the 
//! end of the value. This keeps someone from tricking us into storing
//! something that looks like e.g. a login token as an inoccuous cookie, then 
//! switching the login token cookie and the other cookie, and becoming some
//! other user with a very long TTL.

use warp::{reject, Filter, Rejection, Reply, reply};
use http::{response, header::HeaderValue, status::StatusCode};
use openssl::{sign, error::ErrorStack, pkey::{PKey, Private},
    rand::rand_bytes, hash::{MessageDigest, hash}, memcmp};
use std::thread_local;
use std::cell::RefCell;
use std::error::Error;
use base64::{URL_SAFE, encode_config_buf, decode_config_buf};
use log::*;

thread_local! {
    static SIG: RefCell<(Vec<u8>, Vec<u8>, String)> = 
        RefCell::new((vec![], vec![], "".to_string()));
}

fn num_b64_chars(size: usize) -> usize {
    num_integer::div_ceil(size*4, 3)
}

/// Validates a signed cookie's value.
///
fn validate_cookie(val: &mut String, name: &'static str, signer: &Signer)
    -> Result<bool, Box<dyn Error>> {
    trace!("Validating cookie {}", name);
    trace!("Value: {}", val);
    SIG.with(|sigref| {
        let (ref mut sig, ref mut sig_v, _) = *sigref.borrow_mut();
        let siglen = signer.len();
        let contentlen = val.len() - num_b64_chars(siglen) -1;
        load_sig_buffer(val, name, signer, sig, contentlen)?;
        // Ok, do they match?
        sig_v.resize(sig.len(), 0);
        trace!("sig portion: {}", &val[contentlen..]);
        sig_v.clear();
        decode_config_buf(&val[contentlen..], URL_SAFE, sig_v)?;
        trace!("Resulting len: {}, resulting sig: {:?}", sig_v.len(), &sig_v);
        let m = memcmp::eq(sig, sig_v);
        if m {
            // strip off the hash:
            val.truncate(contentlen);
        }
        Ok(m)
    })
}

/// Extracts and validates the authentication of a secure cookie.
///
/// This filter operatates like
/// `warp::filters::cookie::optional`, except it validates the cookie's name
/// and value were signed by this signer, and it strips the signature metadata.
///
/// It will give Some(String) for a valid cookie, None for an invalid or
/// non-existant cookie, or reject with a SecureError if there is a crypto
/// library error.
pub fn secure_cookie<'b: 'a, 'a>(name: &'static str, signer: &'b Signer)
    -> impl Filter<Extract = (Option<String>,), Error = Rejection> + Copy + 'a {
    warp::cookie::optional(name).and_then(move |cookie: Option<String>| {
        async move {
            let mut val = match cookie {
                Some(s) => s,
                None => {
                    return Ok(None);
                },
            };
            let res = match validate_cookie(&mut val, name, &signer) {
                Ok(true) => Ok(Some(val)),
                Ok(false) => Ok(None),
                Err(e) => {
                    debug!("Rejecting cookie with invalid auth: {}:{}", 
                           name, e.to_string());
                    Err(reject::custom(SecureError{err: e.to_string()}))
                },
            };
            res
        }
    })
}

/// Injects a non-secure cookie of value `val`. This operates like 
/// `warp::reply::WithHeader`, but injects a cookie header.
///
/// `options` should hold a set of already-formatted options like
/// `Secure; HttpOnly` These will be appended to the set-cookie header.
pub fn with_cookie<T: Reply>
(wrap: T, name: &'static str, val: String, options: &'static str)
-> impl Reply {
    WithCookie::Plain {
        name: name,
        val: val,
        wrap: wrap,
        options: options,
    }
}

fn load_sig_buffer(val: &mut String, name: &'static str, signer_o: &Signer,
                   sig: &mut Vec<u8>, contentlen: usize)
    -> Result<(), ErrorStack> {
    let signer = &mut signer_o.get_signer();
    trace!("Signing cookie {}", name);
    // Make sure TLS buffer has enough space for sig
    let siglen = signer.len()?;
    if sig.len() < siglen {
        sig.resize(siglen, 0);
    }
    trace!("Signing {}, bytes: {:?}", &val[..contentlen], 
           val[..contentlen].as_bytes());
    signer.update(&hash(signer_o.digest, &name[..].as_bytes()).unwrap())
        .unwrap();
    signer.update(val[..contentlen].as_bytes()).unwrap();
    let reslen = signer.sign(sig)?;
    sig.truncate(reslen);
    trace!("Binary sig buffer: {:?}", sig);
    Ok(())
}

fn sign_cookie(val: &mut String, name: &'static str, signer_o: &Signer)
    -> Result<(), ErrorStack> {
    trace!("Signing cookie {}", name);
    trace!("Value: {}", val);
    SIG.with(|sigref| {
        let (ref mut sig, _, ref mut sig_b64) = *sigref.borrow_mut();
        load_sig_buffer(val, name, signer_o, sig, val.len())?;
        sig_b64.clear();
        encode_config_buf(sig, URL_SAFE, sig_b64);
        trace!("Signature: {}", sig_b64);
        val.push_str(&sig_b64[..]);
        Ok(())
    })
}

/// Sets a signed cookie with the specifed name, value, and cookie
/// options. Operates like `warp::reply::WithHeader`.
///
/// This signs the cookie body and name, and then appends that signature
/// to the cookie, and then injects it as a Set-Cookie header on the reply.
/// It can be considered the inverse operation of the secure_cookie filter.
///
/// If an error is encountered (for example, with signing the contents), the
/// cookie is not set and an error is logged at the Error level. If an error
/// is encountered creating a header value from the supplied string, the 
/// reply is replaced with a 500 error with a body holding diagnostic 
/// information.
///
/// `options` should hold a set of already-formatted options like
/// `Secure; HttpOnly` These will be appended to the set-cookie header.
pub fn with_cookie_secure<T: Reply>
(wrap: T, name: &'static str, mut val: String, 
 options: &'static str, signer: &Signer)
-> impl Reply {
    let res = sign_cookie(&mut val, name, signer);
    if let Err(t) = res {
        error!("ENCRYPTION FAILED! Cookie: {}, Error: {}", name, t);
        return WithCookie::Without(wrap);
    }
    WithCookie::Plain {
        name: name,
        val: val,
        wrap: wrap,
        options: options,
    }
}

#[derive(Debug)]
enum WithCookie<T: Reply> {
    Plain {
        name: &'static str,
        val: String,
        options: &'static str,
        wrap: T,
    },
    Without(T),
}

impl<T: Reply> From<T> for WithCookie<T> {
    fn from(t: T) -> WithCookie<T> {
        WithCookie::Without(t)
    }
}

impl<T: Reply> Reply for WithCookie<T> {
    fn into_response(self) -> reply::Response {
        match self {
            Self::Plain{name, val, wrap, options} => {
                let mut res = wrap.into_response();
                let cookiestr = format!("{}={}; {}", name, val, options);
                let header = match HeaderValue::from_str(&cookiestr) {
                    Ok(s) => s,
                    Err(e) => {
                        return response::Response::builder()
                            .status(StatusCode::INTERNAL_SERVER_ERROR)
                            .body(e.to_string().into()).unwrap();
                    },
                };
                res.headers_mut()
                    .insert("Set-Cookie", header);
                res
            },
            Self::Without(wrap) => {
                wrap.into_response()
            }
        }
    }
}

/// Abstraction over a OpenSSL Signer.
pub struct Signer {
    key: PKey<Private>,
    digest: MessageDigest,
    len: Option<usize>,
}

impl Signer {
    fn get_signer<'b>(&'b self) -> sign::Signer<'b> {
        return sign::Signer::new(self.digest, &self.key).unwrap();
    }
    fn init(&mut self) {
        let len = self.get_signer().len().unwrap();
        self.len = Some(len);
    }
    fn len(&self) -> usize {
        if let Some(x) = self.len {
            return x;
        } else {
            panic!("Called len on uninitialized signer!");
        }
    }
    /// Creates a new signer from a private key and a digest.
    ///
    /// These two arguments will later be used to construct new
    /// OpenSSL signers using `sign::Signer::new`. To choose them
    /// appropriately, see the rust-openssl docs.
    pub fn new(key: PKey<Private>, digest: MessageDigest) -> Signer {
        let mut s = Signer {
            key: key,
            digest: digest,
            len: None,
        };
        s.init();
        s
    }
    /// Creates sensible default configuration Signer
    ///
    /// Convenience method to get a Signer built around
    /// sha3_256. This is subject to change without notice. Key
    /// is chosen randomly each time this method is run.
    ///
    /// # Panics
    /// Function panics if the underlying crypto functions panic.
    pub fn sensible_default() -> Signer {
        let mut buf = [0; 256];
        rand_bytes(&mut buf).unwrap();
        let key = PKey::hmac(&buf).unwrap();
        let mut s = Signer {
            key: key,
            digest: MessageDigest::sha3_256(),
            len: None,
        };
        s.init();
        s
    }
}


/// Error for requests rejected for malformed authentication
///
/// Simply incorrect authentication or not-present authentication will
/// be represented as None in the filter. However, if something goes
/// very wrong with the crypto library (e.g. exception in the hmac function
/// itself) or values are malformed in such a way that extracting them is
/// impossible, the request will be rejected with this value.
#[derive(Debug)]
pub struct SecureError {
    err: String,
}

impl ToString for SecureError {
    fn to_string(&self) -> String {
        return self.err.clone()
    }
}

impl reject::Reject for SecureError {}

#[cfg(test)]
mod tests {
    use log::*;
    #[test_env_log::test]
    fn roundtrip_encodedecode() {
        let s = crate::Signer::sensible_default();
        let mut val = "foobar0xDEADBEEF".to_string();
        crate::sign_cookie(&mut val, "test", &s).unwrap();
        let verify_correct = crate::validate_cookie(&mut val, "test", &s)
            .unwrap();
        assert!(verify_correct);
        assert_eq!(val, "foobar0xDEADBEEF".to_string());
    }
    #[test_env_log::test]
    fn roundtrip_encodedecode_repeated() {
        let s = crate::Signer::sensible_default();
        let mut val = "foobar0xDEADBEEF".to_string();
        crate::sign_cookie(&mut val, "test", &s).unwrap();
        let verify_correct = crate::validate_cookie(&mut val, "test", &s)
            .unwrap();
        assert!(verify_correct);
        assert_eq!(val, "foobar0xDEADBEEF".to_string());
        let mut val = "foobar0xDEADBABE".to_string();
        crate::sign_cookie(&mut val, "test", &s).unwrap();
        let verify_correct = crate::validate_cookie(&mut val, "test", &s)
            .unwrap();
        assert!(verify_correct);
        assert_eq!(val, "foobar0xDEADBABE".to_string());
        let mut val = "I need test strings".to_string();
        crate::sign_cookie(&mut val, "different", &s).unwrap();
        let verify_correct = crate::validate_cookie(&mut val, "different", &s)
            .unwrap();
        assert!(verify_correct);
        assert_eq!(val, "I need test strings".to_string());
    }
    #[test_env_log::test]
    fn sign_twice() {
        let s = crate::Signer::sensible_default();
        let mut val = "foobar0xDEADBEEF".to_string();
        let mut val2 = val.clone();
        trace!("Val: {}, Val2: {}", val, val2);
        crate::sign_cookie(&mut val, "test", &s).unwrap();
        trace!("Val: {}, Val2: {}", val, val2);
        crate::sign_cookie(&mut val2, "test", &s).unwrap();
        trace!("Val: {}, Val2: {}", val, val2);
        assert_eq!(val, val2);
    }
}
